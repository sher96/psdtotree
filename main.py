# from psd_tools import PSDImage
#
# psd = PSDImage.open('Folder-1/1170 pix Bootstrap Grid.psd')
#
# # print(psd)
#
# for layer in psd:
#     print(layer)
#     if layer.is_group():
#         for child in layer:
#             print(child)
#
# print(psd.tree().export())

from psd_tools.psd import PSD

with open('Folder-1/1170 pix Bootstrap Grid.psd','rb') as f:
    psd = PSD.read(f)

psd.tree().export()
